

import UIKit

class BothVC: UIViewController, UITableViewDelegate, UITableViewDataSource  {

   
    @IBOutlet weak var bothTableView: UITableView!
     var data :[DataResponse] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

         bothTableView.register(MyTableViewCell.nib(), forCellReuseIdentifier: MyTableViewCell.id)
          bothTableView.delegate = self
        bothTableView.dataSource = self
        ApiHelper.getData(type:1) { result in
            switch result {
                
            case .failure(let error):
                        print(error)

        case .success(let value):
            guard let articledata=value.data else {return }
            
            self.data=articledata
                self.setType(type: "Article")
                        ApiHelper.getData(type:2) { result in
                            switch result {
                                
                            case .failure(let error):
                                        print(error)

                        case .success(let value):
                            guard let videodata=value.data else {return }
                                
                            self.data.append(contentsOf: videodata)
                                self.setType(type: "Video")
                           //print( self.data[0].attributes?.card_artwork_url)
                            }
                            self.data = self.data.sorted(by: { ($0.attributes?.released_at ?? "") > ($1.attributes?.released_at ?? "" )})


                         
                           self.bothTableView.reloadData()
                        }
                
                
            }
            
        }
        
        
    }
    func setType(type:String){
        for item in self.data {
            if (item.attributes?.type==nil)
            {item.attributes?.type=type}
            
            
                
        }
        
    }
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.data.count
   }
   
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
       let cell:MyTableViewCell = self.bothTableView.dequeueReusableCell(withIdentifier: MyTableViewCell.id) as! MyTableViewCell
       
       let currentItem = self.data[indexPath.row]
       cell.descriptionLabel.text=currentItem.attributes?.description
          // cell.myCellLabel.text = self.animals[indexPath.row]
       
       cell.artworkImageView.sd_setImage(with: URL(string: (currentItem.attributes?.card_artwork_url)!), placeholderImage: nil)
     
      
       
    
       cell.releaseDateLabel.text!=ApiHelper.convertDate(apiDate:currentItem.attributes?.released_at)
       cell.nameLabel.text=currentItem.attributes?.name
    cell.typeLabel.text=currentItem.attributes?.type
       return cell
   }

    
}
