//
//  MyTableViewCell.swift
//  PVInView
//
//  Created by user176749 on 10/10/20.
//  Copyright © 2020 YOUNGSIC KIM. All rights reserved.
//

import UIKit

class MyTableViewCell: UITableViewCell {

    @IBOutlet weak var releaseDateLabel: UILabel!
    
    
    @IBOutlet weak var artworkImageView: UIImageView!
    
    
    @IBOutlet weak var typeLabel: UILabel!
    
    
    @IBOutlet weak var nameLabel: UILabel!
    
    
    
    @IBOutlet weak var descriptionLabel: UITextView!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static let  id="MyTableViewCell"
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func nib() ->UINib {
        return UINib(nibName: "MyTableViewCell",bundle: nil)
    }
    
}
