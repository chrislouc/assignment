
import UIKit

import SDWebImage


class ArticleVC:  UIViewController, UITableViewDelegate, UITableViewDataSource{

    
    @IBOutlet weak var articleTableView: UITableView!
    var data :[DataResponse] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        articleTableView.register(MyTableViewCell.nib(), forCellReuseIdentifier: MyTableViewCell.id)
        articleTableView.delegate = self
        articleTableView.dataSource = self
       
        
        ApiHelper.getData(type:1) { result in
            switch result {
                
            case .failure(let error):
                        print(error)

        case .success(let value):
            guard let data=value.data else {return }

            self.data=data
           //print( self.data[0].attributes?.card_artwork_url)
            }
              self.data = self.data.sorted(by: { ($0.attributes?.released_at ?? "") > ($1.attributes?.released_at ?? "" )})
            self.articleTableView.reloadData()
        }
        //s Do any additional setup after loading the view.
      
    }

    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:MyTableViewCell = self.articleTableView.dequeueReusableCell(withIdentifier: MyTableViewCell.id) as! MyTableViewCell
        
        let currentArticle = self.data[indexPath.row]
        cell.descriptionLabel.text=currentArticle.attributes?.description
           // cell.myCellLabel.text = self.animals[indexPath.row]
        
        cell.artworkImageView.sd_setImage(with: URL(string: (currentArticle.attributes?.card_artwork_url)!), placeholderImage: nil)
      
       
        
     
        cell.releaseDateLabel.text!=ApiHelper.convertDate(apiDate:currentArticle.attributes?.released_at)
        cell.nameLabel.text=currentArticle.attributes?.name
        cell.typeLabel.text="Article"
        return cell
    }
    
    
    
    
    
    
}


