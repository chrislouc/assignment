

import Foundation
import Alamofire
import ObjectMapper

struct ApiHelper {
   
    
    
    static var githubApiUrl="https://api.github.com/repos/raywenderlich/ios-interview/contents/Practical Example/"
    
   
    static func convertDate(apiDate:String?) -> String {
         guard let apiDate = apiDate else { return "" }
        
        let dateformat = DateFormatter()
        dateformat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateformat.date(from:apiDate)
        dateformat.dateFormat="dd/MM/YYYY HH:mm"
        
        return  dateformat.string(from:date!)
        
        
        
    }


    
    
    static func getData( type:Int,completion: @escaping (AFResult<ParentResponse>)  -> Void) {
        
        let urlstring = self.githubApiUrl.replacingOccurrences(of: " ", with: "%20")
        let request =  AF.request(urlstring)
          
         request.responseDecodable(of: [ApiResponse].self)
            { (response) in
            guard let response = response.value else { return  }
//
                guard let datajsonurl=response[type].download_url else {return }
                
                let datarequest = AF.request(datajsonurl)
                print (datarequest)
                datarequest.responseDecodable (of :ParentResponse.self ) {
                    (dataresponse) in
                     switch dataresponse.result {
                                case .success(let value ):
                                    completion(.success(value))
                    
                                case .failure(let error):
                                    completion(.failure(error))
                    
                                default:
                                    fatalError("received non-dictionary JSON response")
                                }        }
                    
                    
            
                }
                
                
                
              
//
        
        
}
        
//



}









class ApiResponse :Mappable,Decodable
{
    var name:String?
   
    var url: String?
    var html_url: String?
    var git_url : String?
    var download_url: String?
    var type:String?
    
     required init?(map: Map){

    }
    
    func mapping(map: Map) {
       name <- map["name"]
       
        name <- map["name"]
        html_url <- map["html_url"]
        git_url <- map["git_url"]
        download_url <- map["download_url"]
       
        
        
    }
    
}



class ParentResponse :Mappable,Decodable{
    var data:[DataResponse]?
    
    
    required init?(map: Map) {
    
    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
    


}






class DataResponse :Mappable,Decodable{
        var attributes:DataAttributes?
        required init?(map: Map){

           
           }
           
        
        func mapping(map: Map){
            attributes <- map["atributes"]
            
            
        }
        
            

}

class DataAttributes :Mappable,Decodable{
        var uri :String?
        var name: String?
        var description :String?
        var card_artwork_url:String?
        var released_at:String?
        var type:String?
    func mapping(map: Map){
          uri <- map["self"]
           name <- map["git"]
           description <- map["html"]
            card_artwork_url <- map["card_artwork_url"]
            released_at <- map["released_at"]
        }
        
        
        required init?(map: Map){

              
               
           }
           

}




